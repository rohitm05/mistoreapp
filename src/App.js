import PreNavbar from './Component/PreNavbar';
import Navbar from './Component/Navbar';
import Slider from './Component/Slider.js';
import Offer from './Component/Offer.js'
import data from'./data/data.json'
import Heading from './Component/Heading.js'
import './App.css';
import {BrowserRouter as Router} from  'react-router-dom'

function App() {
  return (
    <Router>
        <PreNavbar/>
        <Navbar/>
        <Slider start={data.banner.start}/>
        <Offer offer={data.offer}/>
        <Heading text="STAR PRODUCTS"/>
        <Heading text="HOT ACCESSORIES"/>
      </Router>
  );
}

export default App;
