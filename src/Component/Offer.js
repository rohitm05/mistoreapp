import React from 'react'
import Offers from './Offers.js'
import '../style/Offer.css'
const Offer = ({offer}) => {
    return (
        <div className="offerSection">
           {offer.map((item,index)=>(
            <Offers key={item.image} index={index} src={item.image} link={item.url}/>
           ))} 
        </div>
    )
}

export default Offer
