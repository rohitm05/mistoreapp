import React from 'react'

const Offers = ({src,index,link}) => {
    return (
        
            <a href={link}><img src={src} alt={'offers'}/></a>
    )
}

export default Offers
